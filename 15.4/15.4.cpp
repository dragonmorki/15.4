﻿#include <iostream>

const int N = 20;

void printNumbers(bool even, int n) {
    int start = even ? 0 : 1;
    int inc = even ? 2 : 1;

    for (int i = start; i <= n; i += inc) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

int main() {
    // Выводим чётные числа от 0 до N
    printNumbers(true, N);

    // Выводим нечётные числа от 0 до N
    printNumbers(false, N);

    return 0;
}
